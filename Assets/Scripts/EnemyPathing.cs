﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour
{
    WaveConfig waveConfig;
    List<Transform> waypoints;   
    int waypointIndex;

    void Start()
    {
        waypoints = waveConfig.GetWaypoints;
        transform.position = waypoints[waypointIndex].position;
    }
   

    void Update()
    {
        Move();
    }

    public void SetWaveConfig(WaveConfig waveConfig)
    {
        this.waveConfig = waveConfig;
    }

    private void Move()
    {
        if (waypointIndex < waypoints.Count - 1)
        {
            Vector2 targetPosition = waypoints[waypointIndex + 1].position;
            float movementThisFrame = waveConfig.GetMoveSpeed * Time.deltaTime; //чтобы опять было независимо от фпс компа и скорость была одинаковой
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, movementThisFrame);
            if ((Vector2)transform.position == targetPosition)
            {
                waypointIndex++;
            }
        }
        else
        {
            Destroy(gameObject);
        } 
    }
}
