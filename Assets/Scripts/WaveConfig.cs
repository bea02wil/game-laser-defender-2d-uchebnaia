﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Wave Config")]
public class WaveConfig : ScriptableObject
{
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] GameObject pathPrefab;
    [SerializeField] float timeBetweenSpawns = 0.5f;
    [SerializeField] float spawnRandomFactor = 0.3f;
    [SerializeField] int numberOfEnemies = 5;
    [SerializeField] float moveSpeed = 2f;

    public GameObject GetEnemyPrefab
    {
        get { return enemyPrefab; }
    }

    public List<Transform> GetWaypoints
    {
        get
        {
            List<Transform> waveWaypoints = new List<Transform>();
            foreach (Transform child in pathPrefab.transform)
            {
                waveWaypoints.Add(child);
            }
            return waveWaypoints;
        }
    }

    public float GetTimeBetweenSpawns
    {
        get { return timeBetweenSpawns; }
    }

    public float GetSpawnRandomFactor
    {
        get { return spawnRandomFactor; }
    }

    public int GetNumberOfEnemies
    {
        get { return numberOfEnemies; }
    }

    public float GetMoveSpeed
    {
        get { return moveSpeed; }
    }

    //Стоило лучше поменять названия свойств на названия переменных, только с большой буквы
    //как оно должно быть.
}
